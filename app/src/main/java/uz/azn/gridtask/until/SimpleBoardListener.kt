package uz.azn.gridtask.until

import com.woxthebox.draglistview.BoardView

interface SimpleBoardListener:BoardView.BoardListener{

    override fun onItemDragStarted(column: Int, row: Int) {}

    override fun onItemDragEnded(fromColumn: Int, fromRow: Int, toColumn: Int, toRow: Int) {}

    override fun onItemChangedPosition(oldColumn: Int, oldRow: Int, newColumn: Int, newRow: Int) {}

    override fun onFocusedColumnChanged(oldColumn: Int, newColumn: Int) {}

    override fun onColumnDragStarted(position: Int) {}

    override fun onColumnDragChangedPosition(oldPosition: Int, newPosition: Int) {}

    override fun onColumnDragEnded(position: Int) {}
}