import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.woxthebox.draglistview.BoardView
import com.woxthebox.draglistview.ColumnProperties
import com.woxthebox.draglistview.DragItem
import uz.azn.gridtask.ItemAdapter
import uz.azn.gridtask.R
import uz.azn.gridtask.until.SimpleBoardListener
import java.util.*
import uz.azn.gridtask.databinding.FragmentBoardBinding as Binding

class BoardFragment : Fragment(R.layout.fragment_board) {

    private lateinit var binding: Binding
    private val mGridLayout = true
    private var sCreatedItems = 0
    private var mColumns = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = Binding.bind(view)
        with(binding) {
            with(boardView) {
                setSnapToColumnsWhenScrolling(true)
                setSnapToColumnWhenDragging(true)
                setSnapDragItemToTouch(true)
                setSnapToColumnInLandscape(false)
                setColumnSnapPosition(BoardView.ColumnSnapPosition.CENTER)
                setBoardListener(object : SimpleBoardListener {
                    override fun onItemChangedColumn(oldColumn: Int, newColumn: Int) {
                        with(binding) {
                            val itemCount1 = boardView.getHeaderView(oldColumn).findViewById(R.id.item_count) as TextView
                            itemCount1.text = boardView.getAdapter(oldColumn).itemCount.toString()
                            val itemCount2 = boardView.getHeaderView(newColumn).findViewById(R.id.item_count) as TextView
                            itemCount2.text = boardView.getAdapter(newColumn).itemCount.toString()
                        }
                    }
                })
            }
        }
        resetBoard()
    }

    private fun resetBoard() {
        addColumn()
        addColumn()
        addColumn()
        addColumn()
        addColumn()
    }


    @SuppressLint("SetTextI18n")
    private fun addColumn() {
        val mItemArray = ArrayList<Pair<Long, String>>()
        val addItems = 15
        for (i in 0 until addItems) {
            val id: Long = (sCreatedItems++).toLong()
            mItemArray.add(Pair(id, "Item $id"))
        }
        val listAdapter = ItemAdapter(mItemArray, R.layout.grid_item , R.id.item_layout, true
        )
        val header = View.inflate(activity, R.layout.column_header, null)
        (header.findViewById<View>(R.id.text) as TextView).text = "Column " + (mColumns + 1)
        (header.findViewById<View>(R.id.item_count) as TextView).text = "" + addItems
        header.setOnClickListener { v ->
            val id= (sCreatedItems).toLong()
            val item: Pair<*, *> = Pair(id, "Test $id")
            binding.boardView.addItem(binding.boardView.getColumnOfHeader(v), 0, item, true)

            (header.findViewById<View>(R.id.item_count) as TextView).text = mItemArray.size.toString()
        }

        val layoutManager = if (mGridLayout) GridLayoutManager(context, 4) else LinearLayoutManager(context)
        val columnProperties = ColumnProperties.Builder.newBuilder(listAdapter)
            .setLayoutManager(layoutManager)
            .setHasFixedItemSize(false)
            .setColumnBackgroundColor(Color.TRANSPARENT)
            .setItemsSectionBackgroundColor(Color.TRANSPARENT)
            .setHeader(header)
            .setColumnDragView(header)
            .build()
        binding.boardView.addColumn(columnProperties)
        mColumns++
    }

}
